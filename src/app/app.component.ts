import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { HelloPage } from '../pages/hello/hello';
import { LoginPage } from '../pages/login/login';
import { RegistrationonePage } from '../pages/registrationone/registrationone';
import { RegistrationtwoPage } from '../pages/registrationtwo/registrationtwo';
import { RegistrationthreePage } from '../pages/registrationthree/registrationthree';
import { NewPage } from '../pages/new/new';
import { SelectLanguagePage } from '../pages/select-language/select-language';
import { TranslateService } from '@ngx-translate/core';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = RegistrationonePage;
  //rootPage: any = RegistrationtwoPage;
  //rootPage: any = RegistrationthreePage;
  //rootPage: any = HomePage;
  //rootPage: any = TabsPage;
  rootPage: any = NewPage;
  //rootPage: any = HelloPage;
  //rootPage: any = LoginPage;
  //rootPage: any = SelectLanguagePage;


  pages: Array<{title: string, component: any}>;


  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public translate: TranslateService) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [

      { title: 'Посмотреть заявки', component: HomePage },
      { title: 'Добавить объекты', component: HomePage },
      { title: 'Личный кабинет', component: HomePage },
      { title: 'Режим риэлтора', component: HomePage },
      { title: 'Обратная связь', component: HomePage },
      { title: 'Оставить отзыв', component: HomePage },
      { title: 'Предложить друзьям', component: HomePage },
      { title: 'List', component: ListPage },


    ];



  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.translate.setDefaultLang('ru');
      this.translate.use('ru');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


}
