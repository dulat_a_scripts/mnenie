import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ListPage } from '../pages/list/list';
import { HelloPage } from '../pages/hello/hello';
import { HellotwoPage } from '../pages/hellotwo/hellotwo';
import { HellothreePage } from '../pages/hellothree/hellothree';
import { LoginPage } from '../pages/login/login';
import { RegistrationonePage } from '../pages/registrationone/registrationone';
import { RegistrationtwoPage } from '../pages/registrationtwo/registrationtwo';
import { RegistrationthreePage } from '../pages/registrationthree/registrationthree';
import { SelectLanguagePage } from '../pages/select-language/select-language';
import { NewPage } from '../pages/new/new';




import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//storage
import { IonicStorageModule } from '@ionic/storage';
import { LogicProvider } from '../providers/logic/logic';
//storage
//translate
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ServicesProvider } from '../providers/services/services';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import { NgReduxModule, NgRedux } from '@angular-redux/store'; // <- New


//translate




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    HelloPage,
    HellotwoPage,
    HellothreePage,
    LoginPage,
    RegistrationonePage,
    RegistrationtwoPage,
    RegistrationthreePage,
    SelectLanguagePage,
    TabsPage,
    NewPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{tabsPlacement:"bottom"}),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),

    NgReduxModule, // <- New


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    HelloPage,
    HellotwoPage,
    HellothreePage,
    LoginPage,
    RegistrationonePage,
    RegistrationtwoPage,
    RegistrationthreePage,
    SelectLanguagePage,
    TabsPage,
    NewPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LogicProvider,
    ServicesProvider,

  ]
})
export class AppModule {}
