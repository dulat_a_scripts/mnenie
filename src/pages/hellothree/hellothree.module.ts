import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HellothreePage } from './hellothree';

@NgModule({
  declarations: [
    HellothreePage,
  ],
  imports: [
    IonicPageModule.forChild(HellothreePage),
  ],
})
export class HellothreePageModule {}
