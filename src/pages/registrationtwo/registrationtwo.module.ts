import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationtwoPage } from './registrationtwo';

@NgModule({
  declarations: [
    RegistrationtwoPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationtwoPage),
  ],
})
export class RegistrationtwoPageModule {}
