import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { SmsValidator } from '../../validators/sms.validator';
import { PhoneValidator } from '../../validators/phone.validator';
import { PasswordValidator } from '../../validators/password.validator';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/toPromise';
import { finalize } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { Response } from '@angular/http';
import { LoginPage } from '../login/login' ;





/**
 * Generated class for the RegistrationtwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrationtwo',
  templateUrl: 'registrationtwo.html',
})


export class RegistrationtwoPage {

  todo = {phone:'',email:'', password: '',sms:'' };

  phonereqexp = /^7{1}\d{10}/g;
  emailreqexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  todoForm = new FormGroup({
      'phone': new FormControl(this.todo.phone, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(11),
        Validators.pattern(this.phonereqexp)
      ]),
      'email': new FormControl(this.todo.email, [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(this.emailreqexp)
      ]),
      'password': new FormControl(this.todo.password, [
        Validators.required,
        Validators.minLength(3)
      ]),

    });


    phoneCorrection(event: any) { // without type info

      var phone = event.target.value;
      if(phone.length < 2){
        if(phone == "8"){
          this.todoForm.get('phone').setValue("7");
        }else if(phone == "+"){
          this.todoForm.get('phone').setValue("");
        }
      }
      //console.log(event.target.value);
      //console.log(this.todoForm.get('phone').value);
    }




  validation_messages = {

      'password': [
          { type: 'required', message: 'Password is required.' },
          { type: 'minlength', message: 'min length of this field 3.' },
          { type: 'maxlength', message: 'max length of thid field 11.' },
          { type: 'pattern', message: 'Your password must contain only numbers and letters.' },
          { type: 'validphone', message: 'ok!' }
        ],
        'phone': [
            { type: 'required', message: 'phone is required.' },
            { type: 'minlength', message: 'min length of this field 3.' },
            { type: 'maxlength', message: 'max length of thid field 15.' },
            { type: 'pattern', message: 'Your phone format numbers must be:  "77025557788"' },
            { type: 'validUsername', message: 'ok!' }
          ],
        'email': [
            { type: 'required', message: 'email is required.' },
            { type: 'minlength', message: 'min length of this field 3.' },
            { type: 'maxlength', message: 'max length of thid field 15.' },
            { type: 'pattern', message: 'Your email format string must be:  "xxx@gmail.com"' },
            { type: 'validUsername', message: 'ok!' }
          ],
        'sms': [
            { type: 'required', message: 'Password is required.' },
            { type: 'minlength', message: 'min length of this field 3.' },
            { type: 'maxlength', message: 'max length of thid field 15.' },
            { type: 'pattern', message: 'Your password must contain only numbers and letters.' },
            { type: 'validUsername', message: 'ok!' }
          ],
}

  constructor(public navCtrl: NavController, public navParams: NavParams,public Service:ServicesProvider,private alertCtrl: AlertController,public viewCtrl : ViewController) {


  }

  data:any;



  sendRegistration(){

    //console.log(this.todoForm.value.password);
    let password = this.todoForm.value.password;
    let phone = this.todoForm.value.phone;
    let email = this.todoForm.value.email;

    localStorage.setItem("password",password);
    localStorage.setItem("phone",phone);
    localStorage.setItem("email",email);

      this.Service.sendRegistration(phone,email,password)
        .subscribe(res => {
            this.data = res;
            this.nextPage();
            },
        err => console.log(err),
      );

      // this.Service.sendRegistration(phone,email,password)
      // .first()
      // .toPromise()
      // .then(() => {
      //     this.nextPage();
      //    })
      // .catch((err) => {
      //          return Observable.throw(err)
      //      });

  }

  nextPage(){

    console.log(this.data);

    if(this.data.result == true) {

        localStorage.setItem("id",this.data.Id);
        //localStorage.setItem("password",this.data.Account.Password);
      // localStorage.setItem("sms",this.data.UniCode);
        //this.navCtrl.push(RegistrationthreePage)
        //this.viewCtrl.dismiss();
    } else if(this.data.result == false && this.data.user == "exist_user") {
      return this.presentAlert('This user already exist | Данный пользователь уже существует');
    }else{
      return this.presentAlert('Please check you network connect | Пожалуйста проверьте интернет соединение');
    }

  }

  presentAlert(message){
      let alert = this.alertCtrl.create({
        title: 'Service message',
        subTitle: message,
        buttons: ['Ok']
      });
      alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationtwoPage');
  }

  loginPage(){
    this.navCtrl.push(LoginPage);
  }

}
