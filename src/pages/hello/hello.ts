import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { HellotwoPage } from '../hellotwo/hellotwo';

/**
 * Generated class for the HelloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hello',
  templateUrl: 'hello.html',
})
export class HelloPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl : ViewController) {
  }

  public nextPage(){

      //let myModal = this.modalCtrl.create(HellotwoPage);
      //this.viewCtrl.dismiss();
      this.navCtrl.push(HellotwoPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelloPage');
    console.log(this.navParams.get('message'));
  }

}
