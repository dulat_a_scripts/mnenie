import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrationtwoPage } from '../registrationtwo/registrationtwo';
import { ServicesProvider } from '../../providers/services/services';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/toPromise';
import { finalize } from 'rxjs/operators';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  todo = {email:'',password:''}

  email_phone_reqexp = /^7{1}\d{10}|^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


  todoForm = new FormGroup({
      'email': new FormControl(this.todo.email, [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(this.email_phone_reqexp)
      ]),
      'password': new FormControl(this.todo.password,[
        Validators.required,
        Validators.minLength(3)
      ]),

    });

    phoneCorrection(event: any) { // without type info

      var phone = event.target.value;
      if(phone.length < 2){
        if(phone == "8"){
          this.todoForm.get('email').setValue("7");
        }else if(phone == "+"){
          this.todoForm.get('email').setValue("");
        }
      }
      //console.log(event.target.value);
      //console.log(this.todoForm.get('phone').value);
    }

    validation_messages = {

          'email': [
              { type: 'required', message: 'field email | phone is required.' },
              { type: 'pattern', message: 'email format "abc@gmail.com" | or | phone format "87771119977"' },
              { type: 'validUsername', message: 'ok!' }
            ],
            'password': [
                { type: 'required', message: 'Password is required.' },
                { type: 'minlength', message: 'min length of this field 3.' },
                { type: 'maxlength', message: 'max length of thid field 15.' },
                { type: 'pattern', message: 'Your password must contain only numbers and letters.' },
                { type: 'validUsername', message: 'ok!' }
              ],

  }

  constructor(public navCtrl: NavController, public navParams: NavParams,public Service:ServicesProvider,public viewCtrl : ViewController,private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  data:any;

  logForm() {
    console.log(this.todo);

      this.Service.loginRequest(this.todo)
      .subscribe(res => {
          this.data = res;
          //console.log(res);
          this.nextPage();
          },
      err => console.log(err),
      )


  }


  nextPage(){

    console.log(this.data);

    if(this.data.result == true && this.data.user == "user_found") {

        localStorage.setItem("id",this.data.Id);
        //return this.viewCtrl.dismiss();
        //localStorage.setItem("password",this.data.Account.Password);
      // localStorage.setItem("sms",this.data.UniCode);
       // return this.navCtrl.push(RegistrationthreePage)
    }else if(this.data.user == "incorrect_password"){
      return this.presentAlert("Incorrect password | Пароль не верен");
      //return Observable.empty()
    }else if(this.data.user == "user_not_found"){
      return this.presentAlert("User not found | Пользователь не найден");
    }

  }

  presentAlert(message){
      let alert = this.alertCtrl.create({
        title: 'Service message',
        subTitle: message,
        buttons: ['Ok']
      });
      alert.present();
  }

  regPage(){
    this.navCtrl.push(RegistrationtwoPage);
  }

}
