import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

import { HelloPage } from '../hello/hello';
import { HellotwoPage } from '../hellotwo/hellotwo';
import { LogicProvider } from '../../providers/logic/logic';
import { TranslateService } from '@ngx-translate/core';


import { createStore } from 'redux'




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {

  lang:any;
  icons:any;

  //@ViewChild('mySlider') slider: Slides;
  selectedSegment: string;
  slides: any;

  count: number;


  constructor(public navCtrl: NavController,public modalCtrl: ModalController,public translate: TranslateService) {

    this.selectedSegment = 'first';


    // this.lang = 'ru';
    // this.translate.setDefaultLang('ru');
    // this.translate.use('ru');


  }


  // public switchLanguage() {
  //   this.translate.use(this.lang);
  // }






  public openModal(){

    var data = { message : 'hello world' };
    let myModal = this.modalCtrl.create(HelloPage,data);
    myModal.present();

  }

  onSegmentChanged(segmentButton) {

    console.log(segmentButton.value);

  }

}


// <div *ngIf="hideMe"> here your content</div>
// TS
//
// hide() {
//   this.hideMe = true;
// }
