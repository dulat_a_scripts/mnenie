import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationonePage } from './registrationone';

@NgModule({
  declarations: [
    RegistrationonePage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationonePage),
  ],
})
export class RegistrationonePageModule {}
