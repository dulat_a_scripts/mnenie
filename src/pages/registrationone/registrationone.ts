import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UsernameValidator } from '../../validators/username.validator';
import { PhoneValidator } from '../../validators/phone.validator';
import { PasswordValidator } from '../../validators/password.validator';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/toPromise';
import { finalize } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { RegistrationtwoPage } from '../registrationtwo/registrationtwo';
import { Response } from '@angular/http';


/**
 * Generated class for the RegistrationonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrationone',
  templateUrl: 'registrationone.html',
})
export class RegistrationonePage {

  todo = { phone: ''};
  data: any;
  email = new FormControl('',Validators.compose([
		Validators.required,
		//Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
		//Validators.pattern('^[a-zA-Z0-9_.+-]*@[a-zA-Z0-9-]*.[a-zA-Z0-9-.]+$'),
    Validators.minLength(3),
    Validators.maxLength(15),
    //PhoneValidator.validCountryPhone
    //UsernameValidator.validUsername
	]));

  validation_messages = {

      'email': [
      		{ type: 'required', message: 'Username is required.' },
      		{ type: 'minlength', message: 'min length of this field 3.' },
      		{ type: 'maxlength', message: 'max length of thid field 15.' },
      		{ type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      		{ type: 'validUsername', message: 'ok!' }
      	],
      	'name': [
      		{ type: 'required', message: 'Name is required.' }
      	],
}

//   this.matching_passwords_group = new FormGroup({
// 	password: new FormControl('', Validators.compose([
// 	 	Validators.minLength(5),
// 	 	Validators.required,
// 	 	Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
// 	])),
// 	confirm_password: new FormControl('', Validators.required)
// }, (formGroup: FormGroup) => {
// 	 return PasswordValidator.areEqual(formGroup);
// });

  constructor(public navCtrl: NavController, public navParams: NavParams,public Service:ServicesProvider,private alertCtrl: AlertController) {

  }

  sendSms(){



      let phone = this.todo.phone;

      localStorage.setItem("phone",phone);

      this.Service.getSms(phone)
      .subscribe(res => {
          this.data = res;
          },
      err => console.log(err),
      )

      this.Service.getSms(phone)
      .first()
      .toPromise()
      .then(() => {
          this.nextPage();
         })
      .catch((err) => {
               return Observable.throw(err)
           });

      // this.Service.getSms(this.todo)
      // .first()
      // .subscribe(this.nextPage());

  }

  nextPage(){

    console.log(this.data);

    if(this.data.Result == true) {
      localStorage.setItem("sms",this.data.UniCode);
      return this.navCtrl.push(RegistrationtwoPage)
    } else {
      return this.presentAlert();
      //return Observable.empty()
    }

  }

  presentAlert(){
      let alert = this.alertCtrl.create({
        title: 'Service message',
        subTitle: 'User already exists | Данный пользователь уже зарегистрирован',
        buttons: ['Ok']
      });
      alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationonePage');
  }



}
