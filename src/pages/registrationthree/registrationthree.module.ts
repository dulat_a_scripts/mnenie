import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationthreePage } from './registrationthree';

@NgModule({
  declarations: [
    RegistrationthreePage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationthreePage),
  ],
})
export class RegistrationthreePageModule {}
