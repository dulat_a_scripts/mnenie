import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/toPromise';
import { finalize } from 'rxjs/operators';
import { LoginPage } from '../login/login';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the RegistrationthreePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrationthree',
  templateUrl: 'registrationthree.html',
})
export class RegistrationthreePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public Service:ServicesProvider,public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationthreePage');
  }

  data:any;

  selectPosition(position){

      console.log(position);


      this.Service.updateStatus(position)
      .subscribe(res => {
          this.data = res;
          console.log(res);
          },
      err => console.log(err),
      )

      this.Service.updateStatus(position)
      .first()
      .toPromise()
      .then(() => {
          this.nextPage();
         })
      .catch((err) => {
               return Observable.throw(err)
           });

  }

  nextPage(){

    if(this.data.Result == true){
        return this.viewCtrl.dismiss();
    }

  }

  Login(){
    this.navCtrl.push(LoginPage);
  }

  SkipRegistration(){
    this.viewCtrl.dismiss();
  }

}
