import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { HellothreePage } from '../hellothree/hellothree';

/**
 * Generated class for the HellotwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hellotwo',
  templateUrl: 'hellotwo.html',
})
export class HellotwoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HellotwoPage');
  }

  public nextPage(){

      //let myModal = this.modalCtrl.create(HellotwoPage);
      //this.viewCtrl.dismiss();
      this.navCtrl.push(HellothreePage);
  }

}
