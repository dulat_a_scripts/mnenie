import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HellotwoPage } from './hellotwo';

@NgModule({
  declarations: [
    HellotwoPage,
  ],
  imports: [
    IonicPageModule.forChild(HellotwoPage),
  ],
})
export class HellotwoPageModule {}
