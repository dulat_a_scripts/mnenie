import { HttpClient,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { debounceTime } from 'rxjs/operators/debounceTime';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { share } from "rxjs/operator/share";
import  "rxjs/operator/share";
import 'rxjs/add/observable/throw';



/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//localhost:8100  -- proxy
@Injectable()
export class ServicesProvider {

  //baseurl:string = "http://localhost:3000";
  //baseurl:string = "http://localhost:3000";
  baseurl:string = "http://localhost:8100";

  constructor(public http: HttpClient) {
    console.log('Hello ServicesProvider Provider');
  }


  getSms(phone:String):any{

    console.log(phone);
    return this.http.get(this.baseurl + "/api/registration/" + phone);//izmenit v prodak

  }

  sendRegistration(phone:String,email:String,password:String):any{

    //let phone = localStorage.getItem("phone")

    let json = {
      phone: phone,
      email:email,
      password:password
    }

    console.log(json);

    return this.http.post(this.baseurl + "/api/registration", json);

  }

  updateStatus(status:String):any{

    // <button (click)="selectPosition(3)" ion-button round full>Владелец недвижимости</button>
    // <button (click)="selectPosition(1)" style="margin-top:8px;" ion-button round full>Компания</button>
    // <button (click)="selectPosition(2)" style="margin-top:8px;" ion-button round full>Специалист (Риэлтор)</button>
    // <button (click)="selectPosition(0)" style="margin-top:8px;" ion-button round full>Пользователь</button>

    let urlid = localStorage.getItem("id")

    let json = {
      Status: status,
    }

    console.log(urlid + "|" + status);

    //const headers = new HttpParams().set("Content-Type", "application/json");
    return this.http.put(this.baseurl + "/api/account/status/" + urlid, json);

  }

  loginRequest(todo){

    //console.log(todo);

    let json = {
      email: todo.email,
      password:todo.password
    }

    //console.log(json);

    return this.http.post(this.baseurl + "/api/login", json);

  }



}
