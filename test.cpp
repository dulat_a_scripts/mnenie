#include <map>
#include <limits>
#include <iostream>

using namespace std;

template<typename K, typename V>
class interval_map {

	std::map<K,V> m_map;

public:
    // constructor associates whole range of K with val by inserting (K_min, val)
    // into the map
    interval_map( V const& val) {
        m_map.insert(m_map.end(),std::make_pair(std::numeric_limits<K>::lowest(),val));
    }

    // Assign value val to interval [keyBegin, keyEnd).
    // Overwrite previous values in this interval.
    // Conforming to the C++ Standard Library conventions, the interval
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval,
    // and assign must do nothing.
    void assign( K const& keyBegin, K const& keyEnd, V const& val ) {

			// INSERT YOUR SOLUTION HERE

			if((keyBegin < keyEnd) && (std::numeric_limits<K>::lowest() < keyEnd)){

					typename std::map<K,V>::iterator cycle = m_map.lower_bound(keyBegin);

					if (cycle == m_map.end() && !((--cycle) -> second == val)){

							m_map.insert(m_map.end(), std::make_pair(keyBegin, val));

					}else if(cycle != m_map.end()){

							if(!(keyBegin < cycle -> first)){

									cycle -> second = val;

							}else if((keyBegin < cycle -> first) && !(cycle -> second == val)){

									m_map.insert(--cycle,std::make_pair(keyBegin, val));

							}

					}

			}

    }

    // look-up of the value associated with key
    V const& operator[]( K const& key ) const {
        return ( --m_map.upper_bound(key) )->second;
    }
};

void Interval_Map_Testing()
{

    interval_map <unsigned int,char> mytest('a');

				    mytest.assign(1,2,'a');
				    mytest.assign(2,4,'b');
						mytest.assign(4,6,'d');
				    mytest.assign(6,8,'c');
				    mytest.assign(8,10,'e');
				    mytest.assign(10,12,'e');
				    mytest.assign(12,14,'e');

    cout << " Elements: " << endl << mytest[0] << endl << mytest[1] << endl << mytest[2] << endl << mytest[3] << endl << mytest[4] << endl << mytest[5] << endl << mytest[6] << endl << mytest[7];

}

int main() {

    Interval_Map_Testing();

    return 0;
}

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a test function that tests the functionality of
// the interval_map, for example using a map of unsigned int intervals to char.
