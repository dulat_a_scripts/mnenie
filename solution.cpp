#include <assert.h>
#include <map>
#include <limits>
#include <iostream>

using namespace std;
template<class K, class V>
class interval_map {

    friend void IntervalMapTest();

public:
    std::map<K,V> m_map;

    // constructor associates whole range of K with val by inserting (K_min, val)
    // into the map
    interval_map( V const& val) {
        m_map.insert(m_map.begin(),std::make_pair(std::numeric_limits<K>::lowest(),val));
    };

    // Assign value val to interval [keyBegin, keyEnd).
    // Overwrite previous values in this interval.
    // Do not change values outside this interval.
    // Conforming to the C++ Standard Library conventions, the interval
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval,
    // and assign must do nothing.
    void assign( K const& keyBegin, K const& keyEnd, const V& val ) {
           //Check if assign need to do anything
       if( (keyBegin < keyEnd) && (std::numeric_limits<K>::lowest() < keyEnd ) ){
            typename std::map<K,V>::iterator iter = m_map.lower_bound(keyBegin);
            //Check for consecutive same value. Insert only if different value
            if (iter==m_map.end() && !((--iter)->second == val))
            {
                m_map.insert(m_map.end(),std::make_pair(keyBegin, val));
            }
            else if(iter!=m_map.end())
            {
                if(!(keyBegin < iter->first))
                {
                    iter->second= val;
                }
                else if((keyBegin < iter->first) && !(iter->second == val))
                {
                    m_map.insert(--iter,std::make_pair(keyBegin, val));
                }

            }

        }
    }

    // look-up of the value associated with key
    V const& operator[]( K const& key ) const {
        return ( --m_map.upper_bound(key) )->second;
    }
};

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a function IntervalMapTest() here that tests the
// functionality of the interval_map, for example using a map of unsigned int
// intervals to char.
void IntervalMapTest()
{
    interval_map<unsigned int,char> test('m');
    cout<<"Elements are<<test[0]<<test[1]<<test[2]<<test[3]<<test[4]<<test[5]<<test[6]<<test[7]";
    test.assign(2,4,'k');
    test.assign(4,5,'s');
    test.assign(5,6,'t');
    cout<<"Elements are::::"<<endl<<test[0]<<endl<<test[1]<<endl<<test[2]<<endl<<test[3]<<endl<<test[4]<<endl<<test[5]<<endl<<test[6]<<endl<<test[7];
    //for(auto &iter : test.m_map){cout<<"first"<<iter->first<<"second"<<iter->second;}
}
int main(int argc, char* argv[]) {
    cout<<"hello";
    IntervalMapTest();
    return 0;
}
